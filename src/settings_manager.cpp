#include "settings_manager.h"

#include "hw_includes.h"

static application_settings __attribute__((section(".settings.app")))
settings_placeholder{
    0,   // initial counter value
    0x01 // serial number
};

SharedStorageManager<application_settings, Flash_RW_polcy>
    SettingsManager(&settings_placeholder);
