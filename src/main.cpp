#include <cstring>
#include <stdint.h>

#include "BoardInit.h"
#include "Debug.h"
#include "settings_manager.h"
#include "softreset.h"

#include "LinkInfoStorage/LinkInfoStorage.h"

#include "hw_includes.h"

/*!
 * \brief Remap ISR vectors
 *
 * Copy isr vectors from .isr_vectors to memory _isr_vector_start and set
 * new table as ISR vector table
 */
static void SetISR_Table() {
  extern uint32_t _isr_vector_start;
  extern const uint32_t _isr_vector_end;
  extern const uint32_t __vectors_pos;

  memcpy(&_isr_vector_start, &__vectors_pos,
         (uint32_t)&_isr_vector_end - (uint32_t)&_isr_vector_start);

  SCB->VTOR = (__IOM uint32_t)&_isr_vector_start;
}

int main(void) {
  InitBoard();
  SetISR_Table();

  DEBUG_MSG("Hello from application!");

  auto linkInfo = LinkInfoStorage.load();
  if (!linkInfo.checkConfigured())
    SoftReset();

  auto settings = SettingsManager.load();
  std::get<0>(settings)++;
  SettingsManager.store(settings);

  std::get<0>(settings)++;

  SoftReset();

  // Never going to be here
  return 0;
}
