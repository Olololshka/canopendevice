#ifndef SOFTRESET_H
#define SOFTRESET_H

/*!
 * \addtogroup Utils
 * \brief Reset CPU
 */

#ifdef __cplusplus
extern "C"
#endif

    void __attribute__((noreturn)) SoftReset();

#endif // SOFTRESET_H
