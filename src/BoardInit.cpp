/*!
 * \file
 * \brief Initialization of base MCU peripherals
 */

#include <cstring>

#include "hw_includes.h"

/*!
 * \ingroup BootloaderHW
 *  @{
 */

// Need c++14! (not working as planned!)

/// Class-container of multiplier and divider for PLL
struct sDiv_Mul {
  /// Coded to MCU-specific values divider and multiplier
  uint32_t mul, div;

  /// Empty method to raise compile-time error then divider or multiplier
  /// incorrect
  void error(bool) {}

  /*! Constructor. Set and check coded to MCU-specific values divider and
   * multiplier
   *
   * Raise compile-time error if values out of range
   */
  constexpr sDiv_Mul(int32_t m = -1, int32_t d = -1)
      : mul(static_cast<uint32_t>(m)), div(static_cast<uint32_t>(d)) {
    if (
#if defined(STM32F1) || defined(STM32F3)
        (m < static_cast<int32_t>(RCC_PLL_MUL2)) ||
        (d < static_cast<int32_t>(RCC_HSE_PREDIV_DIV1)) ||
        (m > static_cast<int32_t>(RCC_PLL_MUL16)) ||
#if defined(STM32F105xC) || defined(STM32F107xC) || defined(STM32F100xB) ||    \
    defined(STM32F100xE) || defined(STM32F3)
        (d > static_cast<int32_t>(RCC_HSE_PREDIV_DIV16))
#else
        (d > static_cast<int32_t>(RCC_HSE_PREDIV_DIV2))
#endif

#elif defined(STM32L4)
        // no macro for 8 and 86 defined
        (m < 8) || (d < static_cast<int32_t>(RCC_PLLR_DIV2)) || (m > 86) ||
        (d > static_cast<int32_t>(RCC_PLLR_DIV8))
#endif
    ) {
      error(false);
    }
  }
};

/*!
 * \brief Compile-time calculation of PLL divider and
 * multiplier
 *
 * Calculates proper value of PLL divider and multiplier to achieve CPU clock
 * from HSE value
 * \param div_mul value of Target_cpu_clock/HSE_value
 * \return struct sDiv_Mul, what contains codded PLL divider and
 * multiplier or compile-time error
 */
constexpr sDiv_Mul calc_pll_div_mul(const uint32_t div_mul) {
#if defined(STM32F1) || defined(STM32F3)
  const uint32_t muls[] = {RCC_PLL_MUL2,  RCC_PLL_MUL3,  RCC_PLL_MUL4,
                           RCC_PLL_MUL5,  RCC_PLL_MUL6,  RCC_PLL_MUL7,
                           RCC_PLL_MUL8,  RCC_PLL_MUL9,  RCC_PLL_MUL10,
                           RCC_PLL_MUL11, RCC_PLL_MUL12, RCC_PLL_MUL13,
                           RCC_PLL_MUL14, RCC_PLL_MUL15, RCC_PLL_MUL16};
  const uint32_t divs[] = {
    RCC_HSE_PREDIV_DIV1,
    RCC_HSE_PREDIV_DIV2,
#if defined(STM32F105xC) || defined(STM32F107xC) || defined(STM32F100xB) ||    \
    defined(STM32F100xE) || defined(STM32F3)
    RCC_HSE_PREDIV_DIV3,
    RCC_HSE_PREDIV_DIV4,
    RCC_HSE_PREDIV_DIV5,
    RCC_HSE_PREDIV_DIV6,
    RCC_HSE_PREDIV_DIV7,
    RCC_HSE_PREDIV_DIV8,
    RCC_HSE_PREDIV_DIV9,
    RCC_HSE_PREDIV_DIV10,
    RCC_HSE_PREDIV_DIV11,
    RCC_HSE_PREDIV_DIV12,
    RCC_HSE_PREDIV_DIV13,
    RCC_HSE_PREDIV_DIV14,
    RCC_HSE_PREDIV_DIV15,
    RCC_HSE_PREDIV_DIV16
  };
#endif
  for (uint32_t mul = 2; mul < sizeof(muls) / sizeof(uint32_t) + 2; ++mul) {
    for (uint32_t div = 1; div < sizeof(divs) / sizeof(uint32_t) + 1; ++div) {
      if (mul * 1000 / div == div_mul * 1000) {
        return sDiv_Mul(static_cast<int32_t>(muls[mul - 2]),
                        static_cast<int32_t>(divs[div - 1]));
      }
    }
  }
#elif defined(STM32L4)
  const uint32_t divs[] = {
      RCC_PLLR_DIV2,
      RCC_PLLR_DIV4,
      RCC_PLLR_DIV6,
      RCC_PLLR_DIV8,
  };
  /* PLLN: Multiplication factor for PLL VCO output
    clock. This parameter must be a number between
    Min_Data = 8 and Max_Data = 86 (no macro defined)
  */
  for (uint32_t mul = 8; mul <= 86; ++mul) {
    for (uint32_t div = 0; div < sizeof(divs) / sizeof(uint32_t); ++div) {
      // * 1000 to avoid 8/6 == 1
      if (mul * 1000 / divs[div] == div_mul * 1000) {
        return sDiv_Mul(mul, static_cast<int32_t>(divs[div]));
      }
    }
  }
#else
#error "Unsupported CPU family"
#endif
  return sDiv_Mul();
}

/*!
 * \brief Initialize CPU clicking by setup PLL correct values
 *
 * Uses MACROs HSE_VALUE and F_CPU to calculate and set PLL register values in
 * compile-time.
 * MCU family specific realization
 */
void InitOSC() {
#if defined(STM32F1)
  constexpr auto div_mul = calc_pll_div_mul(F_CPU / HSE_VALUE);
  constexpr RCC_OscInitTypeDef RCC_OscInitStruct = {
      // HSE_VALUE -> PREDIV1 -> PLLMUL => F_CPU
      // PREDIV1 / PLLMUL = F_CPU / HSE_VALUE

      .OscillatorType = RCC_OSCILLATORTYPE_HSE,
      .HSEState = RCC_HSE_ON,
      .HSEPredivValue = div_mul.div,
      .LSEState = RCC_LSE_OFF,
      .HSIState = RCC_HSI_OFF,
      .PLL = {.PLLState = RCC_PLL_ON,
              .PLLSource = RCC_PLLSOURCE_HSE,
              .PLLMUL = div_mul.mul}};

#elif defined(STM32F3)
  constexpr auto div_mul = calc_pll_div_mul(F_CPU / HSE_VALUE);
  constexpr RCC_OscInitTypeDef RCC_OscInitStruct{
      // HSE_VALUE -> PREDIV1 -> PLLMUL => F_CPU
      // PREDIV1 / PLLMUL = F_CPU / HSE_VALUE

      RCC_OSCILLATORTYPE_HSE,
      RCC_HSE_ON,
      div_mul.div,
      RCC_LSE_OFF,
      RCC_HSI_OFF,
      0,
      RCC_LSI_OFF,
      RCC_PLLInitTypeDef{RCC_PLL_ON, RCC_PLLSOURCE_HSE, div_mul.mul}};
#elif defined(STM32L4)

#if F_CPU == HSE_VALUE && 0 // !random hardfaults in this mode!
  constexpr RCC_OscInitTypeDef RCC_OscInitStruct{// HSE_VALUE -> F_CPU
                                                 RCC_OSCILLATORTYPE_HSE,
                                                 RCC_HSE_ON,
                                                 RCC_LSE_OFF,
                                                 RCC_HSI_OFF,
                                                 0,
                                                 RCC_LSI_OFF,
                                                 RCC_MSI_OFF,
                                                 0,
                                                 RCC_MSIRANGE_0,
                                                 RCC_HSI48_OFF,
                                                 RCC_PLLInitTypeDef{}};
#else                       /* F_CPU == HSE_VALUE */
  constexpr auto div_mul = calc_pll_div_mul(F_CPU / HSE_VALUE);
  constexpr RCC_OscInitTypeDef RCC_OscInitStruct{
      // HSE_VALUE -> PLLM (/1) -> PLLN -> PLLR => F_CPU
      // PLLR / PLLN = F_CPU / HSE_VALUE

      RCC_OSCILLATORTYPE_HSE,
      RCC_HSE_ON,
      RCC_LSE_OFF,
      RCC_HSI_OFF,
      0,
      RCC_LSI_OFF,
      RCC_MSI_OFF,
      0,
      RCC_MSIRANGE_0,
      RCC_HSI48_OFF,
      RCC_PLLInitTypeDef{RCC_PLL_ON, RCC_PLLSOURCE_HSE, 1, div_mul.mul,
#if defined(RCC_PLLP_SUPPORT)
                         RCC_PLLP_DIV17,
#endif /* defined(RCC_PLLP_SUPPORT) */
                         RCC_PLLQ_DIV8, div_mul.div}};
#endif /* F_CPU == HSE_VALUE */

#else
#error "Unsupported CPU family"
#endif
  RCC_OscInitTypeDef tmp;
  memcpy(&tmp, &RCC_OscInitStruct, sizeof(RCC_OscInitTypeDef));

  HAL_RCC_OscConfig(&tmp);
}

/*!
 * \brief Setup MCU peripheral buses clocks
 *
 * MCU-family specific realization
 */
void Configure_AHB_Clocks() {
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
                                RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;

#if defined(STM32L4) && (F_CPU == HSE_VALUE) &&                                \
    0 // !random hardfaults in this mode!
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
#else
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
#endif
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;

#if defined(STM32F1) || defined(STM32F3)
  RCC_ClkInitStruct.APB1CLKDivider =
      F_CPU > 36000000U ? RCC_HCLK_DIV2 : RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  auto latency = F_CPU < 24000000U
                     ? FLASH_LATENCY_0
                     : F_CPU < 48000000U ? FLASH_LATENCY_1 : FLASH_LATENCY_2;
#elif defined(STM32L4)
  RCC_ClkInitStruct.APB1CLKDivider =
      F_CPU > 80000000U ? RCC_HCLK_DIV2 : RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  auto latency =
      F_CPU < 16000000U
          ? FLASH_LATENCY_0
          : F_CPU < 32000000U
                ? FLASH_LATENCY_1
                : F_CPU < 48000000U
                      ? FLASH_LATENCY_2
                      : F_CPU < 64000000U ? FLASH_LATENCY_3 : FLASH_LATENCY_4;

#else
#error "Unsupported CPU family"
#endif
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, latency);
}

/*!
 * \brief Configures MCU base clocking and setup system timer
 */
void SystemClock_Config(void) {
  InitOSC();
  Configure_AHB_Clocks();

  // Set up SysTTick to 1 ms
  // HAL
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  // SysTick_IRQn interrupt configuration - setting SysTick as lower priority
  // to satisfy FreeRTOS requirements
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/*!
 * \brief Enable flash protection if release
 */
void flashProtect(void) {
#if defined(APP_FLASH_PROTECT)
  FLASH_OBProgramInitTypeDef OBInit;
  HAL_FLASHEx_OBGetConfig(&OBInit);

  if (OBInit.RDPLevel != OB_RDP_LEVEL_1) {
    OBInit.OptionType = OPTIONBYTE_RDP;
    OBInit.RDPLevel = OB_RDP_LEVEL_1;
    HAL_FLASH_Unlock();
    HAL_FLASH_OB_Unlock();
    HAL_FLASHEx_OBProgram(&OBInit);
    HAL_FLASH_OB_Launch();
    HAL_FLASH_OB_Lock();
    HAL_FLASH_Lock();
  }
#endif
}

/*!
 * \brief Main initialization function
 */
void InitBoard() {
  // Initialize board and HAL
  HAL_Init();

  // defined in HAL_Init()
  // HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  SystemClock_Config();

  flashProtect();

#ifdef __HAL_FLASH_PREFETCH_BUFFER_ENABLE
  __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
#endif

#ifdef __HAL_FLASH_INSTRUCTION_CACHE_ENABLE
  __HAL_FLASH_INSTRUCTION_CACHE_ENABLE();
#endif

#ifdef __HAL_FLASH_DATA_CACHE_ENABLE
  __HAL_FLASH_DATA_CACHE_ENABLE();
#endif
}

/*! @} */
