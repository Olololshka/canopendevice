/*!
 * \file
 * \brief Flas settings storage diffenitions
 */

#ifndef _APP_SETTINGS_H_
#define _APP_SETTINGS_H_

#include "Flash_RW_polcy.h"
#include "Shared_storage.h"
#include "applicationsettingsbase.h"

/// \defgroup Settings Flash settings storage

/*!
 * \ingroup Settings
 * \brief type of settings storage
 *
 * Add more fields to template argument if needed
 */
using application_settings = ApplicationSettingsBase<uint32_t>;

extern SharedStorageManager<application_settings, Flash_RW_polcy>
    SettingsManager;

#endif /* _APP_SETTINGS_H_ */
