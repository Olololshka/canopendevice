
#ifndef FLASH_RW_POLCY_H
#define FLASH_RW_POLCY_H

#include "Shared_storage.h"

/*!
 * \brief MCU internal flash memory read/write policy
 * \ingroup Settings
 *
 * This policy can read and write block of data from specified base address.
 * This base address mast be aligned with MCU flash page size to prevent other
 * data corruption at erasing page
 */
struct Flash_RW_polcy : public MemcpyStoragePolcy {
  /*!
   * \brief Constructor
   *
   * Create new polcy instance with specified base address
   * \param addr Base address, bast be alligned by MCU flash page size
   */
  Flash_RW_polcy(volatile void *addr);

  /*!
   * \brief Erase flash page and writes data to it
   * \param data Pointer to source of data
   * \param size size of data to write
   * \return always true
   */
  bool store(const void *data, size_t size) const;

  /*!
   * \brief Convert size of bytes to flash page count with rounding up
   * \param size Size in bytes
   * \return Page count, what can hold this byte count
   */
  static constexpr size_t size2page_count(size_t size);

private:
  /*!
   * \brief Provide procedure to prepare internal MCU flash to write
   *
   * In STM32 family it unlock flash to write.
   */
  static void flash_programm_start();

  /*!
   * \brief Provide procedure to close flash write session
   *
   * IN STM32 family it lock flash back.
   */
  static void flash_programm_finalise();

  /*!
   * \brief Erase flash from page, contains start_address
   * \param start_address Address (usually firs of target page) to start erase
   * \param length Size in bytes to erase.
   * \return true, on success
   * \warning Before erease flash mast be prepeared by calling
   * flash_programm_start()
   */
  static bool flash_erase(uint32_t start_address, size_t length);

  /*!
   * \brief Write specified data to flash
   * \param dest Destination flash address
   * \param data Pointer to source data
   * \param size Size of data to write
   * \return true, on success
   * \warning to correct write flash must be erased before write
   */
  static bool flash_write(uint32_t dest, const uint8_t *data, size_t size);

  /*!
   * \brief Verify writen data
   * \param dest Destination flash address
   * \param data Pointer to source data
   * \param size Size of data to verify
   * \return true, on success
   */
  static bool verify_writen(uint32_t dest, const uint8_t *data, size_t size);
};

#endif // FLASH_RW_POLCY_H
