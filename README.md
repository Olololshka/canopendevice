# README
CANopen-bootloadable application template for STM32-family MCU.
Used in collaboration with CANopen bootloader https://bitbucket.org/Olololshka/canopenbootloader

### Features
* Executed by CANopenBootloader
* Store setting in MCU flash memory
* Independent of CANopenBootloader code (reset to return to bootloader)
* Own copies of shared libraries (update can't break bootloader)
* Configure against same MCU as bootloader build for

### How to use?
This package is template to write your own application based on CANopenBootloader
Build and install Bootloader before use this package

#### Requirements
* Build dependencies: cmake, git, python2, python3, STM32Cube,
arm-none-eabi-* toolchain with C++17 support, mono
* Optional: doxygen, python-pycrypto, python-hexdump
* Ensure, what -Os specified, use CMAKE_BUILD_TYPE=Debug of Release, not default ""

### Standard usage scenario
Read documentation to CANopenBootloader.

## Building application

### Configuration
* Clone the repository, step into it and initialize it's sub-modules
```#!bash
git clone https://TODO
cd CANopenDevice
git submodule --init update
```

* Create sub directory for building, step into it and run cmake generator,
provide path to CANopenBootloader install dir by -D option.
```#!bash
mkdir build && cd build
cmake .. -DCANopenBootloader_DIR=<path to directory contains CANopenBootloaderConfig.cmake>
```

* Configure application by cmake-gui
```#!bash
cmake-gui .
```

* In displayed window check flags *Grouped* and *Advanced*
    Parameters description
    * CLOCK_F_CPU - application cpu clock
    * CANOPEN_VENDOR_ID - application World-unique vendor ID
    * CANOPEN_PRODUCT_ID - application vendor specific product ID
    * BUILD_USE_LTO - use link-time optimizations

**BEWARE TO EDIT OTHER PARAMETERS**

### build

Execute **make** to build your application.
In subdirectory **src** application elf image will be generated
In subdirectory **hybrid** will be generated two executables:
    * **hybrid.bootloader** - bootloader image with whole application code injected in.    contains all bootloader debug symbols
    * **hybrid.app** - application image with bootloader injected in. Сan be used to debug application with correct start from bootloader
